package com.ady.betterinterface

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar


class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        //1 - Configuring Toolbar
        configureToolbar()
    }

    private fun configureToolbar() {
        //Get the toolbar (Serialise)
        val toolbar: Toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        //Set the toolbar
        setSupportActionBar(toolbar)
        // Get a support ActionBar corresponding to this toolbar
        val ab: ActionBar? = supportActionBar
        // Enable the Up button
        ab?.setDisplayHomeAsUpEnabled(true)
    }
}
