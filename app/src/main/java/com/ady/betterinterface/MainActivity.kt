package com.ady.betterinterface

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar


class MainActivity : AppCompatActivity() {
    private var imageViewOC: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Configuring Toolbar
        configureToolbar()
        // Serialise & configure imageView
        this.configureImageView();
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu and add it to the Toolbar
        menuInflater.inflate(R.menu.menu_activity_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle actions on menu items
        return when (item.itemId) {
            R.id.menu_activity_main_params -> {
                Toast.makeText(
                    this,
                    "Il n'y a rien à paramétrer ici, passez votre chemin...",
                    Toast.LENGTH_LONG
                ).show()
                true
            }
            R.id.menu_activity_main_search -> {
                Toast.makeText(
                    this,
                    "Recherche indisponible, demandez plutôt l'avis de DuckDuckGo, c'est mieux et plus rapide.",
                    Toast.LENGTH_LONG
                ).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun configureToolbar() {
        // Get the toolbar view inside the activity layout
        val toolbar: Toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        // Sets the Toolbar
        setSupportActionBar(toolbar)
    }

    private fun configureImageView() {
        // Serialise ImageView
        imageViewOC = findViewById<ImageView>(R.id.imageView)
        // Set OnClick Listener on it
        imageViewOC!!.setOnClickListener { // Launch Detail Activity
            launchDetailActivity()
        }
    }

    private fun launchDetailActivity() {
        val myIntent = Intent(this@MainActivity, DetailActivity::class.java)
        this.startActivity(myIntent)
    }
}
